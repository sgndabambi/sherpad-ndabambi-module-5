import 'package:flutter/material.dart';
import 'package:sherpad_ndabambi_module_4/dashboard.dart';
import 'package:sherpad_ndabambi_module_4/signup.dart';

class LogInPage extends StatelessWidget {
  const LogInPage({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(title)),
        body: Column(
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 16),
              child: Image(
                image: AssetImage('assets/todo-svgrepo-com.png'),
                width: 64,
              ),
            ),
            Text(
              "Sign in to $title",
              textScaleFactor: 2,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(64, 48, 64, 0),
              child: Column(
                children: [
                  TextFormField(
                    decoration: const InputDecoration(
                      labelText: 'Enter your email address',
                    ),
                  ),
                  const SizedBox(height: 32),
                  TextFormField(
                    decoration: const InputDecoration(
                      labelText: 'Enter your password',
                    ),
                    obscureText: true,
                  ),
                  const SizedBox(height: 32),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Dashboard(
                                  title: title,
                                )),
                      );
                    },
                    style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 32, vertical: 24),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50),
                        )),
                    child: const Text('Sign in'),
                  ),
                  const SizedBox(
                    height: 32,
                  ),
                  const Text("Don't have an account?"),
                  const SizedBox(
                    height: 16,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SignUpPage(
                                  title: title,
                                )),
                      );
                    },
                    style: ElevatedButton.styleFrom(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 32, vertical: 24),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50),
                      ),
                    ),
                    child: const Text("Sign up"),
                  )
                ],
              ),
            ),
          ],
        ));
  }
}
