// @dart=2.9
import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'colors.dart';
import 'package:sherpad_ndabambi_module_4/login.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);
  final title = "Assignment Tracker";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: title,
      home: SplashScreen(
        backgroundColor: secondary,
        image: Image.asset('assets/todo-svgrepo-com.png'),
        loaderColor: primary,
        navigateAfterSeconds: LogInPage(title: title),
        photoSize: 100,
        seconds: 2,
        title: Text(
          title,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20.0,
          ),
        ),
      ),
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(primarySwatch: primary)
            .copyWith(secondary: primary),
        floatingActionButtonTheme:
            const FloatingActionButtonThemeData(backgroundColor: Colors.green),
        iconTheme: IconThemeData(color: primary),
        inputDecorationTheme: InputDecorationTheme(
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(color: primary.shade400),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(color: primary),
          ),
          labelStyle: TextStyle(
            color: primary[700],
            fontSize: 14,
          ),
        ),
        scaffoldBackgroundColor: secondary,
        textTheme: TextTheme(
          bodyMedium: TextStyle(color: primary[800]),
        ),
      ),
    );
  }
}
