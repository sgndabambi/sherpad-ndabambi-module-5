import 'package:flutter/material.dart';
import 'package:sherpad_ndabambi_module_4/colors.dart';

class EditProfile extends StatelessWidget {
  const EditProfile({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Column(
        children: [
          Row(
            children: const [
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(top: 16),
                  child: Icon(
                    Icons.account_circle,
                    size: 64,
                  ),
                ),
              ),
            ],
          ),
          const Text(
            "Edit profile",
            textScaleFactor: 2,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(64, 16, 64, 0),
            child: Column(
              children: [
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Enter new first name',
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: primary.shade400),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: primary),
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Enter new last name',
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: primary.shade400),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: primary),
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Enter new email address',
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: primary.shade400),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: primary),
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Enter new password',
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: primary.shade400),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: primary,
                      ),
                    ),
                  ),
                  obscureText: true,
                ),
                const SizedBox(height: 16),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Confirm new password',
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: primary.shade400),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: primary,
                      ),
                    ),
                  ),
                  obscureText: true,
                ),
                const SizedBox(height: 48),
                ElevatedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 32, vertical: 24),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50),
                    ),
                  ),
                  child: const Text('Save changes'),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
