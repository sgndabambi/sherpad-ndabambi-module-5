import 'package:flutter/material.dart';

MaterialColor primary = const MaterialColor(
  0xff363853,
  <int, Color>{
    50: Color.fromRGBO(54, 56, 83, .1),
    100: Color.fromRGBO(54, 56, 83, .2),
    200: Color.fromRGBO(54, 56, 83, .3),
    300: Color.fromRGBO(54, 56, 83, .4),
    400: Color.fromRGBO(54, 56, 83, .5),
    500: Color.fromRGBO(54, 56, 83, .6),
    600: Color.fromRGBO(54, 56, 83, .7),
    700: Color.fromRGBO(54, 56, 83, .8),
    800: Color.fromRGBO(54, 56, 83, .9),
    900: Color.fromRGBO(54, 56, 83, 1),
  },
);

MaterialColor secondary = const MaterialColor(
  0xffcfdfff,
  <int, Color>{
    50: Color.fromRGBO(191, 239, 255, .1),
    100: Color.fromRGBO(191, 239, 255, .2),
    200: Color.fromRGBO(191, 239, 255, .3),
    300: Color.fromRGBO(191, 239, 255, .4),
    400: Color.fromRGBO(191, 239, 255, .5),
    500: Color.fromRGBO(191, 239, 255, .6),
    600: Color.fromRGBO(191, 239, 255, .7),
    700: Color.fromRGBO(191, 239, 255, .8),
    800: Color.fromRGBO(191, 239, 255, .9),
    900: Color.fromRGBO(191, 239, 255, 1),
  },
);
